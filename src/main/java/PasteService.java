import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class PasteService {

    private static final String PASTE_SERVICE_URL = "https://pastebin.com/";
    private static final String pasteCode = "git config --global user.name  \"New Sheriff in Town\"" + "\n" +
                                            "git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")" + "\n" +
                                            "git push origin master --force";
    private static final String DROPDOWN_VALUE = "//li[text()='%s']";
    private static final String expiration = "10 Minutes";
    private static final String format = "Bash";
    private static final String pasteTitle = "how to gain dominance among developers";

    private WebDriver driver;

    @FindBy(xpath = "//*[@class='qc-cmp2-summary-info ']")
    private WebElement popupWindow;

    @FindBy(xpath = "//span[text()='AGREE']")
    private List<WebElement> popupCookiesAgreeBtn;

    @FindBy(id = "postform-text")
    private WebElement pasteForm;

    @FindBy(id = "select2-postform-format-container")
    private WebElement dropdownSyntaxHighlighting;

    @FindBy(id = "select2-postform-expiration-container")
    private WebElement dropdownExpiration;

    @FindBy(id = "postform-name")
    private WebElement pasteTitleForm;

    @FindBy(xpath = "//*[@class=\"btn -big\"]")
    private WebElement createNewPaste;

    @FindBy(xpath = "//div[@class=\"info-top\"]/h1")
    private WebElement newPasteTitle;

    @FindBy(xpath = "//*[@href=\"/archive/bash\"]")
    private WebElement newPasteSyntaxHighlighting;

    @FindBy(xpath = "//a[text()='raw']")
    private WebElement rawCode;

    @FindBy(xpath = "//pre")
    private WebElement newPasteCode;

    boolean isNewPasteTitleMatch;
    boolean isNewPasteSyntaxHighlightingMatch;
    boolean isNewPasteCodeMatch;

    public PasteService(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    private WebElement waitForElementToBeClickable(WebElement element) {
        return new WebDriverWait(driver, Duration.ofSeconds(10)).
                until(ExpectedConditions.elementToBeClickable(element));
    }

    public PasteService openPasteService(){
        driver.get(PASTE_SERVICE_URL);
        return this;
    }

    public PasteService closePopupCookies(){
        if(popupCookiesAgreeBtn.size() > 0){
            popupCookiesAgreeBtn.get(0).click();}
        return this;
    }

    public PasteService addCode(){
        pasteForm.sendKeys(pasteCode);
        return this;
    }

    public PasteService setSyntaxHighlighting() {
        waitForElementToBeClickable(dropdownSyntaxHighlighting);
        dropdownSyntaxHighlighting.click();
        WebElement syntaxHighlightingValue = driver.findElement(By.xpath(String.format(DROPDOWN_VALUE,format)));
        syntaxHighlightingValue.click();
        return this;
    }

    public PasteService setExpiration() {
        waitForElementToBeClickable(dropdownExpiration);
        dropdownExpiration.click();
        WebElement expirationDropdownValue = driver.findElement(By.xpath(String.format(DROPDOWN_VALUE,expiration)));
        expirationDropdownValue.click();
        return this;
    }

    public PasteService addTitle(){
        pasteTitleForm.sendKeys(pasteTitle);
        return this;
    }

    public PasteService createNewPaste(){
        waitForElementToBeClickable(createNewPaste);
        createNewPaste.click();
        return this;
    }

    public boolean checkNewPasteTitleMatch(){
        if(newPasteTitle.getText().equalsIgnoreCase(pasteTitle)){
            isNewPasteTitleMatch = true;
        }
        return isNewPasteTitleMatch;
    }

    public boolean checkNewPasteSyntaxHighlighitingMatch(){
        if(newPasteSyntaxHighlighting.getText().equals(format)){
            isNewPasteSyntaxHighlightingMatch = true;
        }
        return isNewPasteSyntaxHighlightingMatch;
    }

   public boolean checkNewPasteCodeMatch(){
       waitForElementToBeClickable(rawCode);
       rawCode.click();
       if(newPasteCode.getText().trim().equals(pasteCode.trim())){
           isNewPasteCodeMatch = true;}
       return isNewPasteCodeMatch;
        }
    }

