import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.junit.Assert;

public class PasteServiceTest {
    private WebDriver driver;

    @Test
    public void newPasteService() {

        driver = new ChromeDriver();

        PasteService newPaste = new PasteService(driver).
                openPasteService().
                closePopupCookies().
                addCode().
                setSyntaxHighlighting().
                setExpiration().
                addTitle().
                createNewPaste();


        boolean newPasteTitleMatch = newPaste.checkNewPasteTitleMatch();
        boolean newPasteSyntaxHighlightingMatch = newPaste.checkNewPasteSyntaxHighlighitingMatch();
        boolean newPasteCodeMatch = newPaste.checkNewPasteCodeMatch();

        driver.close();
        driver.quit();

        Assert.assertTrue("New paste title doesn't match", newPasteTitleMatch);
        Assert.assertTrue("New paste code doesn't match", newPasteCodeMatch);
        Assert.assertTrue("New paste format doesn't match", newPasteSyntaxHighlightingMatch);
    }
}